/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    Navigator
} from 'react-native';
import {Container, Header,Text, Title, Content, Footer, FooterTab, Button, Left, Right, Body, Icon} from 'native-base';
import {StackNavigator} from 'react-navigation';

//import screens
import MainScreen from './app/MainScreen.js';
import WebViewScreen from './app/WebViewScreen.js';
import SearchScreen from './app/SearchScreen.js';

const ttyouhui = StackNavigator({
		MainScreen: {screen: MainScreen},
		WebViewScreen: {screen: WebViewScreen},
		SearchScreen: {screen: SearchScreen},
},{
		headerMode:"none"
}

);

AppRegistry.registerComponent('ttyouhui', () => ttyouhui);
