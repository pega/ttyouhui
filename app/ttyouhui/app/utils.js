/**
 * ShopReactNative
 *
 * @author Tony Wong
 * @date 2016-08-13
 * @email 908601756@qq.com
 * @copyright Copyright © 2016 EleTeam
 * @license The MIT License (MIT)
 */

'use strict';
//const ApiBaseUrl="http://192.168.1.200/youhuiquan/server/";
//const ApiBaseUrl="http://172.16.90.156/youhuiquan/server/";
const ApiBaseUrl="http://demo2016.wx.hansap.net.cn/youhuiquan/";
// import {FormData} from 'react-native';
let Util = {

    /**
     * http get 请求简单封装
     * @param url 请求的URL
     * @param successCallback 请求成功回调
     * @param failCallback 请求失败回调
     */
    get: (url, successCallback, failCallback) => {
				//console.warn(this.ApiBaseUrl+url);
        fetch(ApiBaseUrl+url)
            .then((response) => response.text())
            .then((responseText) => {
                let result = JSON.parse(responseText);
								successCallback(result);
                //successCallback(result.status, result.code, result.message, result.data, result.share);
            })
            .catch((err) => {
                failCallback(err);
            });
    },

    /**
     * http post 请求简单封装
     * @param url 请求的URL
     * @param data post的数据
     * @param successCallback 请求成功回调
     * @param failCallback failCallback 请求失败回调
     */
    post: (url, data, successCallback, failCallback) => {
        let formData = new FormData();
        Object.keys(data).map(function(key) {
            var value = data[key];
            formData.append(key, value);
        });

        let fetchOptions = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                // 'Content-Type': 'application/json'
                'Content-Type': 'multipart/form-data',
            },
            body: formData
            // body: JSON.stringify(data)
        };

        fetch(ApiBaseUrl+url, fetchOptions)
            .then((response) => response.text())
            .then((responseText) => {
                let result = JSON.parse(responseText);
								successCallback(result);
                //successCallback(result.status, result.code, result.message, result.data, result.share);
            })
            .catch((err) => {
                failCallback(err);
            });
    },

    /**
     * 日志打印
     * @param obj
     */
    log: (obj) => {
        var description = "";
        for(let i in obj){
            let property = obj[i];
            description += i + " = " + property + "\n";
        }
        alert(description);
    },

		getBannerParams: (id)=>{
				var params = [
						{page: {title:"韩都衣舍",url:"https://s.click.taobao.com/t?e=m%3D2%26s%3DmtcOlhEaLQIcQipKwQzePDAVflQIoZepK7Vc7tFgwiFRAdhuF14FMTT2WAwVRoi01aH1Hk3GeOhl7q6IfhDBD0jj6qD1QN09JMQEnw2ahHq3URWtH7v2c3QRaEJbaM%2BtIYULNg46oBA%3D"}},
						{page: {title:"天猫超市-今日疯抢 多款好货任你抢 低价1元起",url:"https://s.click.taobao.com/t?e=m%3D2%26s%3Da2bVZk8qBEMcQipKwQzePCperVdZeJviK7Vc7tFgwiFRAdhuF14FMW7G%2B1dfG89F1aH1Hk3GeOhl7q6IfhDBD0jj6qD1QN09JMQEnw2ahHryZ7qdhyisxaUuZxIcp9pfUIgVEmFmgnaR4ypTBJBwtCFRZMSvwpawPAVey3Kvb3YW95D0t3aOipLIwx8%2BuZb78nvnsZfes3glfg41X90OHae32d3ezLIV61vtbMsgODVdITvXwTxvKFx%2FkB%2FXuumO"}},
						{page: {title:"IT爱好者",url:"https://s.click.taobao.com/t?e=m%3D2%26s%3DK%2F0HReALKescQipKwQzePCperVdZeJviK7Vc7tFgwiFRAdhuF14FMe02vgsYAlFrRitN3%2FurF3xl7q6IfhDBD0jj6qD1QN09JMQEnw2ahHryZ7qdhyisxaUuZxIcp9pfUIgVEmFmgnaR4ypTBJBwtLCfUhe%2B6x3KiBqx4AoGTRwTMkUtS6f4UWqFT2L8zA8AIEEA5re2a34AdO5LZxrFmUnJcmSQ8QiVj1M2MW%2FMeS0n1WSbch1S%2BeZ%2Bqo1O84tsNYYHskMCeArLP4XO97Tuio%2BgRw6KoLAkxiXvDf8DaRs%3D"}},
				]
				return params[id];
		}
};

export default Util;
