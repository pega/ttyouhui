import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
		Image,
    TouchableHighlight,
		Alert
} from 'react-native';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Toast,
    List,
    ListItem,
    Thumbnail,
    Text,
    Body,
    Spinner,
		Drawer
} from 'native-base';
import Video from 'react-native-video';
import styles from './styles.js';
import Swiper from 'react-native-swiper';
import CouponItem from './components/CouponItem.js';
import TwoCoupons from './components/TwoCoupons.js';
import SideBar from './components/SideBar.js';
import Util from './utils.js';

export default class SearchScreen extends Component {
		constructor(props) {
				super(props);
				console.log("SearchScreen constructor",this.props.navigation.state.params);
				this.state = {
					keyword:this.props.navigation.state.params.search.keyword,
					category:this.props.navigation.state.params.search.category,
					searchResult: [],
					twoCouponsList:[],
					pageNumber: 1,
					pageSize: 10,
					showToast: false,
					showLoading:false,
					twoItemsOneRow:true,
					noMore:false,
					total:-1
				};
		}

		componentDidMount = function (){
				this.search();
		}

		static navigationOptions = {
        title: 'SearchScreen'
    };

		goBack = function (){
				this.props.navigation.goBack();
		};

		goWebview = function (coupon){
				this.props.navigation.navigate("WebViewScreen",{coupon: coupon});
		};

		search = function (loadMore=false){
				this.setState({showLoading:true});
				let keyword = this.state.keyword;
				let category = this.state.category;
				let pageNumber = this.state.pageNumber;
				let pageSize = this.state.pageSize;
				let url = "index.php?a=index&pageSize="+pageSize+"&pageNumber="+pageNumber;
				if(keyword){
						url = url+"&keyword="+keyword;
				}
				if(category){
						url = url+"&category="+category;
				}
				Util.get(url,(response) =>{
						//this.state.suggestResult
						console.log(response);
						let towCouponsTemp = [];
						if(loadMore==true){
								towCouponsTemp = this.state.twoCouponsList;
						}

						for(let i=0;i<response.data.items.length/2;i++){
								//response.data[i*2].onPress = this.goWebview(response.data[i*2]);
								//response.data[i*2+1].onPress = this.goWebview(response.data[i*2+1]);
								towCouponsTemp.push(
										[response.data.items[i*2],response.data.items[i*2+1]]
								);
						}

						this.setState({showLoading:false,searchResult:response.data.items,twoCouponsList:towCouponsTemp,total:response.data.total});
				},(error)=>{
						this.setState({showLoading:false});
						console.warn(error);
				});

		};

		setCurrentReadOffset = (event) => {
				let itemHeight = styles.couponItemFrame.height;
				let currentOffset = Math.floor(event.nativeEvent.contentOffset.y);
				let currentItemIndex = Math.ceil(currentOffset / itemHeight);
				//console.log(event.nativeEvent.contentOffset.y);
				console.log("currentItemIndex:"+currentItemIndex);
				//console.log("height:",styles.couponItemFrame.height);

				if(currentItemIndex+1>=this.state.twoCouponsList.length){
						if(!this.state.noMore){

								if(!this.state.showLoading){
										console.log("load more",this.state.pageNumber);
										this.setState({pageNumber:this.state.pageNumber+1});
										this.search(true);
								}else{
										console.log("loading");
								}

								if(this.state.total>0&&this.state.twoCouponsList.length*2>=this.state.total){
										this.setState({noMore:true});
								}
						}else{
								console.log("no more ");
						}
				}
  	}

		render() {
        const {navigate} = this.props.navigation;
				return (
						<Container>
							<Header style={styles.header} >
								<Left>
									<Button transparent onPress={() => this.goBack()}>
										<Icon style={styles.white}  name='arrow-back' />
									</Button>
								</Left>
								<Body >
									<Title style={styles.headerTitle}>天天优惠 {this.state.keyword}</Title>
								</Body>
								<Right>

								</Right>
							</Header>
							<Content
								onScroll={this.setCurrentReadOffset}
								scrollEventThrottle={1000}
								>
								<Item style={styles.searchBox}>
										<Input style={styles.searchInput}
											placeholder='搜索看看'
											onChangeText={(keyword) => this.setState({keyword})}
											returnKeyType='done'
											onSubmitEditing={() => this.search() }
											value={this.state.keyword}/>
										<Icon active name='search' onPress={() => this.search() } />
								</Item>

								{this.state.twoItemsOneRow &&
									<List dataArray={this.state.twoCouponsList}
												renderRow={(twoCoupons) =>
													<ListItem style={styles.listItem}>
															<TwoCoupons twoCoupons={twoCoupons} onPress={() => this.goWebview(twoCoupons[0])} onPress2={() => this.goWebview(twoCoupons[1])}></TwoCoupons>
													</ListItem>
											}
									>
									</List>
								}
								{!this.state.twoItemsOneRow &&
									<View style={{flex: 0, flexDirection: 'row', flexWrap: 'wrap'}}>
											<List dataArray={this.state.searchResult}
														renderRow={(item) =>
															<ListItem style={{marginLeft:0,borderBottomWidth:0,paddingRight:0}}>
																	<CouponItem coupon={item} onPress={() => this.goWebview(item)} ></CouponItem>
															</ListItem>
													}
											>

											</List>
									</View>
								}
								{ this.state.showLoading &&
								<Spinner color='blue' />
								}
								{this.state.noMore &&
									<Text style={styles.noMoreText}>我是有底线的</Text>
								}
							</Content>
						</Container>
				);
		}
}
