/**
 * Created by sami on 30/05/2017.
 */
import {Dimensions,StyleSheet} from 'react-native';
/*
export default styles = StyleSheet.create({
    fullScreen: {
        position: 'absolute',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    nativeVideoControls: {
        //top: 184,
        height: 300
    },
    webView: {
        backgroundColor: 'rgba(255,255,255,0.8)',
    },
		mheader: {
				backgroundColor: '#f8285c',
		},
		slider: {
				backgroundColor: '#d1d1d1',
				height: 300
		},
		container: {
	      flex: 1,
	      backgroundColor: 'green',
    },
		searchBox: {
				borderRadius: 3,
				borderWidth: 0.5,
				borderColor: '#f8285c',
		}
});
*/
const {screenHeight, screenWidth} = Dimensions.get('window');
export default {
	header:{
			backgroundColor: '#f8285c'
	},
	banner:{
			width:screenHeight,
			height:145
	},
	logo:{
			width:48,
			height:24
	},
	searchBox: {
			borderRadius: 3,
			borderWidth: 0.5,
			borderLeftWidth: 0.5,
			borderRightWidth: 0.5,
			borderTopWidth: 0.5,
			borderBottomWidth: 0.5,
			borderColor: '#f8285c',
			marginTop:7,
			marginBottom:7,

	},
	searchInput:{
			height:40
	},
	headerTitle:{
			width:200,
			fontSize:18,
			color:'#ffffff'
	},
	white:{
			color:'#ffffff'
	},
	listItem:{
			marginLeft:0,
			borderBottomWidth:0,
			paddingRight:0,
			paddingTop:0,
			paddingBottom:0,
			marginBottom:15
	},
	noMoreText:{
			fontSize:10,
			textAlign:'center',
			lineHeight:20,
			marginBottom:10
	},
	couponItemFrame:{
			width: 180,height:240,backgroundColor:'#ffffff'
	},
	couponItemImage:{
			width: 180,height:180
	},
	couponItemRow:{
			flex: 0, flexDirection: 'row', flexWrap: 'nowrap'
	},
	couponItemIcon:{
			width: 14,height:14,marginTop:2,marginRight:2
	},
	couponItemIconText:{
			width:30,fontSize:12,lineHeight:20,marginRight:2
	},
	couponItemText1:{
			width:150,fontSize:12,lineHeight:20
	},
	couponItemText2:{
			width:100,fontSize:14,color:'#f8285c',lineHeight:20
	},
	couponItemText3:{
			width:80,fontSize:12,color:'#f8285c',lineHeight:20
	},
	couponItemText4:{
			fontSize:12
	}
}
