/**
 * Created by sami on 30/05/2017.
 */
import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
		WebView,
		Dimensions,
    TouchableHighlight,
		Linking,
		Share
} from 'react-native';
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Icon,
    Item,
    Input,
    Toast,
    List,
    ListItem,
    Thumbnail,
    Text,
    Body,
    Spinner
} from 'native-base';
import Video from 'react-native-video';
import styles from './styles.js';
import Swiper from 'react-native-swiper';

export default class WebviewScreen extends Component {
		webview = null;

    constructor(props) {
        super(props);
				console.log("WebviewScreen constructor",this.props.navigation.state.params);
				let webViewInfo = {title:"天天优惠",url:""};
				let coupon = null;
				let page = null;
				if(this.props.navigation.state.params.coupon){
						webViewInfo.title = this.props.navigation.state.params.coupon.title;
						webViewInfo.url = this.props.navigation.state.params.coupon.coupon_click_url;
						coupon = this.props.navigation.state.params.coupon;
				}else if(this.props.navigation.state.params.page){
						webViewInfo.title = this.props.navigation.state.params.page.title;
						webViewInfo.url = this.props.navigation.state.params.page.url;
						page = this.props.navigation.state.params.page;
				}
        this.state = {webViewHeight:600, showLoading:false,coupon:coupon,page:page,webViewInfo:webViewInfo};
    }

		static navigationOptions = {
				title: 'WebView',
		};

		goBack = function (){
				this.props.navigation.goBack();
		};

		refresh = function (){
				console.log("refresh");
				this.webview.reload();
		};

		_updateWebViewHeight = function(event){
				//console.log(event);
				//console.warn("_updateWebViewHeight event.jsEvaluationValue",event.jsEvaluationValue);
				//console.warn(Dimensions.get('window').height,Dimensions.get('window').width);
				if(event.jsEvaluationValue){
				 		this.setState({webViewHeight: parseInt(event.jsEvaluationValue)});
				}else{
						this.setState({webViewHeight:600});
				}

		};

		// onError={this._onError.bind(this)}
		// renderError={this._renderError.bind(this)}
		_onError = function(error){
				console.log("_onError",error);
		}

		_renderError = function(error){
				console.log("_renderError",error);
		}

		componentDidMount() {
		  	Linking.addEventListener('url', this._handleOpenURL);
		}

		componentWillUnmount() {
		  	Linking.removeEventListener('url', this._handleOpenURL);
		}

		_handleOpenURL = function(event) {
		  	console.log(event.url);
		}

		share = function(){
				let shareConfig = {
						message:this.state.webViewInfo.title+" "+this.state.webViewInfo.url,
						url:this.state.webViewInfo.url,
						title:this.state.webViewInfo.title
				};

				Share.share(shareConfig, {
		      dialogTitle: shareConfig.title,
		      tintColor: 'red'
		    })
		    .then((result)=>console.log(result))
		    .catch((error)=>console.log(error));
		}

		render() {
				const {navigate} = this.props.navigation;

				return (
						<Container>
							<Header style={styles.header}>
								<Left>
									<Button transparent onPress={() => this.goBack()}>
										<Icon name='arrow-back' />
									</Button>
								</Left>
								<Body>
									<Title style={styles.headerTitle}>{this.state.webViewInfo.title}</Title>
								</Body>
								<Right>
									<Button transparent onPress={() => this.refresh()}>
										<Icon name='refresh' />
									</Button>
									<Button transparent onPress={() => this.share()}>
										<Icon name='share' />
									</Button>
								</Right>
							</Header>
							<Content>
								<WebView
									ref={webview => { this.webview = webview; }}
									source={{uri: this.state.webViewInfo.url}}
									injectedJavaScript="document.body.scrollHeight;"
                  scrollEnabled={false}
                  onNavigationStateChange={this._updateWebViewHeight.bind(this)}
                  automaticallyAdjustContentInsets={true}
                  style={{width: Dimensions.get('window').width, height: this.state.webViewHeight}}

								/>
							</Content>
						</Container>
				);
		}
}
