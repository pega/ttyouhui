import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
		Image,
    TouchableHighlight,
} from 'react-native';
import {
    Content,
    Text,
} from 'native-base';
import CouponItem from './CouponItem.js';
export default class TwoCoupons extends Component {
	constructor(props) {
			super(props);
	}

	render() {
			return (
				<View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap',justifyContent: 'space-between',}}>
						<CouponItem coupon={this.props.twoCoupons[0]} onPress={() => this.props.onPress()}></CouponItem>
						{this.props.twoCoupons[1] &&
						<CouponItem coupon={this.props.twoCoupons[1]} onPress={() => this.props.onPress2()} ></CouponItem>
						}
				</View>
			)
	}
}
