import React, { Component } from 'react';
import { Container, Content, List, ListItem, Text,Button } from 'native-base';
export default class SideBar extends Component {
	constructor(props) {
		super(props);
		this.state = {
			shadowOffsetWidth: 1,
			shadowRadius: 4,
		};
	}

	goCategory = function(category,keyword=null){
			this.props.navigation.navigate('SearchScreen',{search:{category:category,keyword:keyword}});
	}

	render() {
    return (
      <Container style={{backgroundColor: '#ffffff'}}>
        <Content>
          <List>
            <ListItem itemHeader first>
              <Text>分类</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('女装') }>
              <Text>女装</Text>
            </ListItem>
            <ListItem  onPress={() => this.goCategory('居家') }>
              <Text>家居用品</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('鞋') }>
              <Text>鞋</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('包') }>
              <Text>包</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('食品') }>
              <Text>食品</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('数码') }>
              <Text>3C数码</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('汽车') }>
              <Text>车品配件</Text>
            </ListItem>
						<ListItem  onPress={() => this.goCategory('运动') }>
              <Text>运动户外</Text>
            </ListItem>
          </List>
        </Content>
      </Container>
    );
  }
}
