import React, {Component, PropTypes} from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
		TouchableHighlight,
} from 'react-native';
import {
    Content,
    Text,
		Spinner
} from 'native-base';
import Image from 'react-native-image-progress';
import styles from '../styles.js';

export default class CouponItem extends Component {
	constructor(props) {
			super(props);
			//console.warn(props);
			//this.setState({keyword: "2", searchResult: [], page: 0, showToast: false});
	}

	renderUserTypeIcon = function(userType){
			if(userType=="天猫"){
					return <Image style={styles.couponItemIcon} source={require('../images/tmall.png')}/> ;
			}else if (userType=="淘宝") {
					return <Image style={styles.couponItemIcon} source={require('../images/taobao.png')}/> ;
			}else{
					return <Text style={styles.couponItemIconText}>{this.props.coupon.user_type}</Text> ;
			}
	}

	render() {
			return (
					<View style={styles.couponItemFrame} >
							<TouchableHighlight onPress={() => this.props.onPress()}>
							<Image
								style={styles.couponItemImage}
								source={{uri:this.props.coupon.pict_url}}
								indicator={()=><Spinner color='red' />}
							/>
							</TouchableHighlight>
							<View style={styles.couponItemRow}>
								{this.renderUserTypeIcon(this.props.coupon.user_type)}
								<Text style={styles.couponItemText1} numberOfLines={1}>{this.props.coupon.title}</Text>
							</View>
							<View style={styles.couponItemRow}>
								<Text style={styles.couponItemText2}>¥{this.props.coupon.zk_final_price}(券后)</Text>
								<Text numberOfLines={1} style={styles.couponItemText3}>{this.props.coupon.coupon_info}</Text>
							</View>
							<View style={styles.couponItemRow}>
								<Text style={styles.couponItemText4}>已有{this.props.coupon.volume}人购买</Text>
							</View>

					</View>
			)
	}
}
