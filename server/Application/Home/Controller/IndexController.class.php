<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function index(){
				$pageNumber = intval(I("get.pageNumber",1));
				$pageSize = intval(I("get.pageSize",10));
				if($pageSize>200){
						$pageSize = 200;
				}
				$keyword = I("get.keyword");
				$category = I("get.category");
				$map = array();
				if($keyword){
						$map['title'] = array('like',"%".$keyword."%");
				}
				if($category){
						$map['category'] = array('like',"%".$category."%");
				}

				$list = M("youhuiquan")->where($map)->order("id DESC")->page($pageNumber,$pageSize)->select();
				if(empty($list)){
						$list = array();
				}
				$total = M("youhuiquan")->where($map)->count();
				$total = intval($total);
				//echo M()->getLastSql();
				if($list){
				 		$this->jsonResult(
								array(
										"items"=>$list,
										"total"=>$total,
								)
						);
				}else{
				 		$this->jsonResult(
								array(
										"items"=>$list,
										"total"=>$total,
								)
						);
			 	}
		}

		public function suggest(){
				// $list = M("youhuiquan")->where("title like '%家居%'")->order("id DESC")->limit(30)->select();
				// M("suggest")->addAll($list);
				// die;
				$list = M("suggest")->order("id DESC")->select();
				if($list){
				 		$this->jsonResult($list);
				}else{
				 		$this->jsonResult(array(),0,"server error");
			 	}
		}

		public function version(){
				$version = array(
						"ANDROID_VERSION"=>C("ANDROID_VERSION"),
						"IOS_VERSION"=>C("IOS_VERSION"),
				);
				$this->jsonResult($version);
		}

		public function ad(){
				$postion = I("get.postion");
				//$ad position width height mediaType mediaUrl screen params
				//mediaType image video
				//screen WebViewScreen SearchScreen SettingScreen Linking
				$ad = array(
						"position"=>"APP_HOME",
						"width"=>375,
						"height"=>145,
						"mediaType"=>"image",
						"mediaUrl"=>"https://static.oschina.net/uploads/cover/2686220_lsBet_bi.png",
						"screen"=>"WebViewScreen",
						"params"=>array(
								"page"=>array(
										"title"=>"天天",
										"url"=>"http://placeimg.com/750/290/any",
								)
						),
				);

				$ad1 = $ad;
				$ad1["mediaUrl"] = "https://i.h2.pdim.gs/efe72225babcb2683f66183cc3064d0b.jpeg";
				$ad2 = $ad;
				$ad2["mediaUrl"] = "https://i.h2.pdim.gs/d57bea51e85d027dde73e96c9fe1830d.png";
				$list = array($ad,$ad1,$ad2);
				$this->jsonResult($list);
		}

		public function addSuggest(){
				$list = M("youhuiquan")->where("title like '%家居%'")->order("id DESC")->limit(30)->select();
				$r = M("suggest")->addAll($list);
				dump($r);
				die;
		}

		public function import(){
				if(PHP_SAPI!="cli"){
						echo "only in cli";
						exit();
				}

				global $argv;

				if(empty($argv[2])){
						echo "input file path empty";
						exit();
				}


				$file = fopen($argv[2],"r");
				if(!$file){
						echo "fail to open file";
						die;
				}
				dump($file);
				echo "\r";
				fgetcsv($file);
				$line = 1;
				while(!feof($file)){
						$tmp = iconvArray(fgetcsv($file));
						$data = array(
								"num_iid"=>$tmp[0],
								"title"=>$tmp[1],
								"pict_url"=>$tmp[2],
								"item_url"=>$tmp[3],
								"category"=>$tmp[4],
								"taoke_url"=>$tmp[5],
								"price"=>$tmp[6],
								"volume"=>$tmp[7],
								"commission_rate"=>$tmp[8],
								"commission"=>$tmp[9],
								"nick"=>$tmp[10],
								"seller_id"=>$tmp[11],
								"shop_title"=>$tmp[12],
								"user_type"=>$tmp[13],
								"coupon_id"=>$tmp[14],
								"coupon_total_count"=>$tmp[15],
								"coupon_remain_count"=>$tmp[16],
								"coupon_info"=>$tmp[17],
								"coupon_start_time"=>$tmp[18],
								"coupon_end_time"=>$tmp[19],
								"coupon_url"=>$tmp[20],
								"coupon_click_url"=>$tmp[21],
						);

						$data["zk_final_price"] = calDiscountPrice($data["price"],$data["coupon_info"]);

						$id = M("youhuiquan")->where(array("num_iid"=>$data["num_iid"]))->getField("id");
						if($id){
								$data["update_time"] = date("Y-m-d h:i:s");
								$r = M("youhuiquan")->where(array("id"=>$id))->save($data);
								echo "line:{$line} update id:{$id} \r\n";
						}else{
								$data["create_time"] = date("Y-m-d h:i:s");
								$data["update_time"] = date("Y-m-d h:i:s");
								$r = M("youhuiquan")->add($data);
								echo "line:{$line} insertid:{$r} \r\n";
						}
						$line++;
						// if($line>5){
						//  		break;
						// }
				}
				fclose($file);
		}

		public function test(){
				$str1 = "满129元减120元";
				$str2 = "5元无条件券";
				$str3 = "PHP is the web scripting language of choice.";
				//$reg1 = "^\w+\d\w+\dw+$";
				//$reg1 = "/满\d*元减\d*元/";
				// $reg1 = "/满\d*元减\d*元/";
				// $r = preg_match($reg1,$str1,$match);
				// dump($r);
				// dump($match);

				$res = calDiscountPrice(200,$str1);
				dump($res);
				$res = calDiscountPrice(200,$str2);
				dump($res);
		}

		private function jsonResult($data,$status=1,$message=""){
				header('Content-type: application/json');
				echo json_encode(
						array(
								"data"=>$data,
								"status"=>$status,
								"message"=>$message
						)
				);
				exit();
		}
}
