<?php
function iconvArray($array){
		$res = array();
		foreach ($array as $key => $value) {
				$res[$key] = iconv('GB2312', 'UTF-8', $value);
		}
		return $res;
}

function calDiscountPrice($price,$couponInfo){
		$reg1 = "/满\d+元减\d+元/";
		$reg2 = "/\d+元无条件券/";
		$regInt = "/\d+/";
		$result = $price;
		if(preg_match($reg1,$couponInfo)){
				preg_match_all($regInt,$couponInfo,$match);
				$p1 = $match[0][0];
				$p2 = $match[0][1];
				if($price>=$p1){
						$result = $result - $p2;
				}
		}else if(preg_match($reg2,$couponInfo)){
				preg_match_all($regInt,$couponInfo,$match);
				$p1 = $match[0][0];
				$result = $result - $p1;
		}
		return $result;
}
