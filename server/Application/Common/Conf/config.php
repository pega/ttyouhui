<?php
return array(
	      'DB_TYPE'   => 'mysql', // 数据库类型
        'DB_HOST'    => '127.0.0.1', //数据库地址
        'DB_NAME'    => 'test', //数据库SID
        'DB_USER'   => 'root', // 用户名
        'DB_PWD'    => '', // 密码
        'DB_PREFIX' => '', // 数据库表前缀
        'DB_PARAMS' => array(PDO::ATTR_CASE => PDO::CASE_NATURAL),
				'URL_MODEL' => 0
);
